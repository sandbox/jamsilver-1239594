<?php

/**
 * Search API data alteration callback that adds an URL field for all items.
 */
class DomainSearchAlterAddDomain extends SearchApiAbstractAlterCallback {

  public function alterItems(array &$items) {
    foreach ($items as &$item) {

      // Store each associated domain
      // We deliberately maintain any pre-existing value in $item->domain_search_domains
      // therefore other modules can implement hook_search_api_index_items_alter()
      // and seed our array with whatever.
      $item->domain_search_domains = isset($item->domain_search_domains) ? $item->domain_search_domains : NULL;

      if (!empty($item->domains) && is_array($item->domains)) {
        $item->domain_search_domains = isset($item->domain_search_domains) ? $item->domain_search_domains : array();
        foreach ($item->domains as $domain_id) {
          $domain = domain_lookup($domain_id);
          $item->domain_search_domains[$domain['subdomain']] = $domain['subdomain'];
        }
      }

      // Store whether or not this content should be available for all domains
      $item->domain_search_all = !empty($item->domain_site) ? 1 : 0;
    }
  }

  public function propertyInfo() {
    return array(
      'domain_search_domains' => array(
        'label' => t('Domain'),
        'description' => t('The domain that the content belongs to.'),
        'type' => 'list<string>',
      ),
      'domain_search_all' => array(
        'label' => t('All Domains'),
        'description' => t('Indicate that this content should be accessible accross all domains.'),
        'type' => 'boolean',
      ),
    );
  }

}
